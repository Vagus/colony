class Projectile:
	nProjectiles = 0
	projectiles = []
	def __init__(self, startPos, targetPos, currentPos, target, owner, damage, speed=1):
		self.startPos = startPos
		self.targetPos = targetPos
		self.currentPos = currentPos
		self.target = target
		self.owner = owner
		self.damage = damage
		self.speed = speed
		self.projectiles.append(self)
		self.nProjectiles += 1
	def __del__(self):
		Projectile.nProjectiles = Projectile.nProjectiles - 1
		
class Bullet(Projectile):
	def __init__(self, startPos, targetPos, currentPos, target, owner, image, damage = 10, speed=1):
		super().__init__(startPos, targetPos, currentPos, target, owner, damage, speed)
		self.image = image