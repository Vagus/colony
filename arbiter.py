from colonist import Colonist
from conf import *
from shoot import *
from job import Job
from tile import Tile
from build import *
import math
from ai import *

def doJob(colonist):
    # nothing to do
    if colonist.job == None:

        print("colonist " + colonist.firstName + " no job")
        return False
    elif colonist.job.jobType == 'build':
        doBuild(colonist)
    elif colonist.job.jobType == 'fight':
        if colonist.team == 0:
            doFight(colonist)
        else:
            doEnemyFight(colonist)
            # doEnemyFight(colonist)
            # doFight(colonist)
    # has to move to subject
    elif colonist.job.status == 'New':
        if colonist.job.jobType == 'wander':
            doMove(colonist, Tile.getRandomReachableTile(colonist.position))
        elif colonist.job.jobType == 'move':
            doMove(colonist, colonist.job.position)  
    elif colonist.job.status == 'Moving':
        if colonist.job.jobType == 'wander' or colonist.job.jobType == 'move':
            # finished moving to subject
            if colonist.job.position == colonist.position:
                print("finished moving")
                colonist.job = None
                if len(colonist.jobQueue) > 0:
                    colonist.jobQueue.pop(0)
                # colonist.job.status = 'done'
                if len(colonist.jobQueue) > 0:
                    colonist.job = colonist.jobQueue[0]
            
            # else:
            #     print("keep moving")
            #     doMove(colonist, colonist.job.position)
    else:
        print ("job failed")
        colonist.job = None
        return False

def doBuild(colonist):
    # has to move to subject
    if colonist.job.status == 'New':
        print ("moving to build")
        
        if not math.floor(Tile.getEuclidianDistance(colonist.job.subject.position, colonist.position)) == 1:
            closestTile = Tile.getClosestWalkableTile(colonist.job.subject.position)
            if closestTile:
                Colonist.setColonistDestination(colonist, closestTile)
            if colonist.path:
                colonist.job.status = 'Moving'
                return True
            else:
                print ("unreachable")
                colonist.job.status = 'Unreachable'
                colonist.job.worker = None
                colonist.job = None
                return False
        else:
            print("close enough")
            colonist.job.status = 'Building'
    elif colonist.job.status == 'Moving':
        print("moving")
        print(math.floor(Tile.getEuclidianDistance(colonist.job.subject.position, colonist.position)))
        if math.floor(Tile.getEuclidianDistance(colonist.job.subject.position, colonist.position)) == 1:
            print("close enough")
            colonist.job.status = 'Building'
    elif colonist.job.status == 'Building':
        if math.floor(Tile.getEuclidianDistance(colonist.job.subject.position, colonist.position)) == 1:
            finished = advanceConstruction(colonist.job.subject, Colonist.baseColonistBuildingSpeed)
            if finished:
                colonist.job.status = 'Done'
                colonist.job.worker = None
                colonist.job = None
        else:
            colonist.job.status = 'New'
    else:
        print ("job failed")
        colonist.job.worker = None
        colonist.job = None
        return False

def doFight(colonist):
    pass
    # print("fight")
    # if enemyInRange(colonist):
    #     shoot(colonist)
    # else:
    # print("too far")
    # coverTile = findCover(colonist)
    # if coverTile:
    #     colonist.jobQueue.insert(0, Job.createJob ('move', coverTile, 'New') )
    #     colonist.job = colonist.jobQueue[0]
    #     colonist.job.subject = coverTile
            # doMove(colonist, colonist.job.subject)

def moveToCover(colonist, coverTile):
    if not coverTile == colonist.position:
        colonist.jobQueue.insert(0, Job.createJob ('move', coverTile, 'New', coverTile, colonist, colonist.team) )
        colonist.job = colonist.jobQueue[0]
        colonist.job.position = coverTile
        doMove(colonist, colonist.job.position)

def doEnemyFight(colonist):
    if colonist.job.jobType == "move":
        print("fight moving")
        if validateCover(colonist, colonist.job.position):
            doMove(colonist, colonist.job.subject)
        else:
            coverTile = findCover(colonist)
            if coverTile:
                moveToCover(colonist, coverTile)
            else:
               if enemyInRange(colonist) and checkVisibility(colonist, colonist.enemy):
                shoot(colonist) 
    else:
        if validateCover(colonist, colonist.position):
            # print ("cover valid")
            if enemyInRange(colonist) and checkVisibility(colonist, colonist.enemy):
                shoot(colonist)
        else:
            print ("cover invalid")
            coverTile = findCover(colonist)
            if coverTile:
                moveToCover(colonist, coverTile)
            else:
                print("failed to find cover")

def doMove(colonist, destination):
    if not destination == colonist.position:
        colonist.job.status = 'Moving'
        # print ("tile " + str(destination.tileNumber) + " is mine " + colonist.firstName)
        
        Colonist.setColonistDestination(colonist, destination)
    else:
        colonist.jobQueue.pop(0)
        colonist.job = colonist.jobQueue[0]
        destination.colonistDestination = None


def validateJob(colonist):
    if colonist.fighting:
        if colonist.enemy and colonist.enemy.health > 0:
            return True
        else:
            return False
    elif colonist.job.jobType == 'move':
        if Tile.isReachable(colonist.position, colonist.job.subject):
            return True
        else:
            
            return False
    else:
        return True
def getVisibleEnemies(colonist):
    enemiesAround = []
    for otherColonist in Colonist.colonists:
        if not otherColonist == colonist:
            if not otherColonist.team == colonist.team:
                if otherColonist.health > 0:
                    if checkVisibility(colonist, otherColonist):
                        distance = Tile.getEuclidianDistance(colonist.position, otherColonist.position)
                        enemiesAround.append((otherColonist, distance))
    return enemiesAround

def arbiter():
    for colonist in Colonist.colonists:
        if colonist.health > 0:
            if colonist.fighting:
                if not validateJob(colonist):
                    colonist.fighting = False
                    colonist.job = None
                    colonist.jobQueue.clear()
                else:
                    doJob(colonist)
                    continue
            else:
                # see if theres enemies around
                enemiesAround = getVisibleEnemies(colonist)
                if colonist.team == 1 and len(enemiesAround) > 0: 
                    def sortByDistance(x):
                        return x[1] 
                    enemiesAround.sort(key=sortByDistance)
                    enemy, _ = enemiesAround[0]
                    colonist.job = Job.createJob ('fight', enemy, 'New', None, colonist, colonist.team)  
                    colonist.jobQueue.clear()
                    colonist.jobQueue.insert(0, colonist.job )
                    colonist.enemy = enemy
                    colonist.fighting = True
                    # print(colonist.firstName + " du mydlit " + enemy.firstName)
                # if safe, continue job
                else:
                    if not colonist.job == None:
                        if not validateJob(colonist):
                            if len(colonist.jobQueue) > 0:
                                colonist.jobQueue.pop(0)
                            print("invalid " + colonist.job.jobType)

                # if still no job, pick new one
                if colonist.job == None:
                    if len(colonist.jobQueue) > 0:
                        colonist.job = colonist.jobQueue[0]
                    elif len(Job.jobs) > 0:
                        for job in Job.jobs:
                            if job.worker == None and not job.status == "Done" and job.team == colonist.team:
                                if job.jobType == "build":
                                    job.worker = colonist
                                    colonist.job = job
                                    print("new job " + job.jobType)
                                    break
                # if still no job, wander
                if colonist.job == None:
                    print("new wander")
                    destination = Tile.getRandomReachableTile(colonist.position)
                    colonist.job = Job.createJob ('wander', destination, 'New', destination, colonist, colonist.team)
            doJob(colonist)