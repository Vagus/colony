import random
from astar import astar_search
from conf import *
from tile import Tile
import pygame
from weapon import *

class Colonist:
	nColonists = 0
	colonists = []
	deadColonists = []
	baseColonistSpeed = 0.05
	baseColonistBuildingSpeed = 1
	def __init__(self, firstName, image, position, team, visualPosition = None, job = None, weapon = None, \
		 jobQueue = [], moving = False, path = [], health = 20000, fighting = False, enemy = None):
		self.firstName = firstName
		self.image = image
		self.job = job
		self.weapon = weapon
		self.team = team
		# self.last = last
		# self.nickname = nickname
		self.position = position
		self.visualPosition = visualPosition
		self.jobQueue = jobQueue
		self.moving = moving
		self.path = path
		self.health = health
		self.fighting = fighting
		self.enemy = enemy
		Colonist.nColonists += 1
		self.colonists.append(self)

	def __del__(self):
		pass

	def killColonist(colonist, images):
		# Colonist.deadColonists.append(colonist)
		# Colonist.colonists.remove(colonist)
		Colonist.nColonists -= 1
		
		colonist.image = 'dead'
		colonist.job = None
		colonist.weapon = None
		colonist.jobQueue = []
		colonist.moving = False
		colonist.path = []
		colonist.position.colonistStanding = None
		if colonist in colonistsToMove:
			colonistsToMove.remove(colonist)

	def colonistManager(images):
		for colonist in Colonist.colonists:
			if colonist.health <= 0:
				Colonist.killColonist(colonist, images)

	def spawnColonist(tileSize):
		def selectRandomTile():
			for _ in range(30):
				randomTile = random.choice(list(Tile.tiles))
				if randomTile.colonistStanding == None and randomTile.structure == None:
					return randomTile
				else:
					print('Failed to find tile')
		spawnTile = selectRandomTile()
		colonist1 = Colonist('Erika', "chick_big", spawnTile, 0)
		colonist1.team = 0
		spawnTile.colonistStanding = colonist1
		colonist1.visualPosition = (colonist1.position.column * tileSize - Tile.relCornerX , colonist1.position.row * tileSize - Tile.relCornerY)
		colonist1.weapon = Gauss()
		# colonist2 = Colonist('Martoun', "capybara", random.choice(list(Tile.tiles)))
		# colonist2.visualPosition = (colonist2.position.row * tileSize, colonist2.position.column * tileSize)
		animals = ["alpaca", "bull", "sexy-cow", "fox", "pig", "sheep"]
		for x in range(extraColonists):
			spawnTile = selectRandomTile()
			x = Colonist(str(Colonist.nColonists), random.choice(animals), spawnTile, 1)
			spawnTile.colonistStanding = x
			x.team = 1
			x.weapon = Sniper()
			x.visualPosition = colonist1.visualPosition

	def changeColonist():
		pass

	def moveColonist(colonist, _tileX):
		_previousTile = colonist.position
		if _previousTile.colonistStanding == colonist:
			_previousTile.colonistStanding = None
		colonist.position = _tileX
		# print(_tile)
		_tileX.colonistStanding = colonist

	def setColonistDestination(colonist, tile):
		tileSize = Tile.tileSize
		startTile = colonist.position
		endTile = tile
		
		pathTiles, path = astar_search(startTile, endTile)
		if pathTiles:
			# for tile in pathTiles:
				# dotsToDraw.append(tile)
			if not colonist in colonistsToMove:
				colonistsToMove.append(colonist)
			colonist.moving = True
			colonist.path = path
			activePaths[colonist] = colonist.path
			colonist.visualPosition = (colonist.position.column * tileSize, colonist.position.row * tileSize)
			startTile.colonistOnTile = None
			endTile.colonistOnTile = colonist
			return pathTiles, path
		else:
			colonist.path = None
			return None, None

	def traversePath(colonistsToMove):
		tileSize = Tile.tileSize
		stepSize = tileSize * Colonist.baseColonistSpeed
		# orientations = {(1,1):(-1, -1, "vlevo nahoru"), (1,0):(-1, 0, "doleva"),
					 # (1,-1):(-1, 1, "doleva dolu"), (0,1):(0, -1, "nahoru"), (0,-1):(0, +1, "dolu"),
					  # (-1,1):(+1, -1, "doprava nahoru"), (-1,0):(1, 0, "doprava"), (-1,-1):(1, 1, "doprava dolu")}
		for colonist in colonistsToMove:
			path = colonist.path
			mark = False
			# no path found
			if not path:
				print("no path")
				colonistsToMove.remove(colonist)
				colonist.activity = None
				colonist.path = None
				break
			# reached end of path
			if path and colonist.position == path[-1]:
				colonist.moving = False
				activePaths[colonist] = None
				colonist.activity = None
				colonist.path = None
				colonistsToMove.remove(colonist)
				# print("reached end")
				return False
			for node in path:
				if mark:
					currentTile = colonist.position
					nextTile = node
					# tilesChanged.append(Tile.tilesXY(colonist.visualPosition[0] // tileSize, colonist.visualPosition[1] // tileSize))
					visualPosition = colonist.visualPosition
					length = pygame.Vector2(visualPosition[0] - nextTile.column * tileSize, visualPosition[1] - nextTile.row * tileSize).length()
					if length < stepSize:
						stepSize = length
						# print("overshoot")
					# step = orientations[(currentTile.row - nextTile.row, currentTile.column - nextTile.column)]
					step = (nextTile.column - currentTile.column, nextTile.row - currentTile.row)
					visualPosition = [visualPosition[0] + step[0] * stepSize, visualPosition[1] + step[1] * stepSize]
					colonist.visualPosition = visualPosition

					neighborXY = [(currentTile.column + 1, currentTile.row + 1), (currentTile.column + 1, currentTile.row), (currentTile.column + 1, currentTile.row - 1),
					(currentTile.column, currentTile.row + 1),(currentTile.column, currentTile.row - 1),
					(currentTile.column - 1, currentTile.row + 1), (currentTile.column - 1, currentTile.row), (currentTile.column - 1, currentTile.row -1)]

					for i in neighborXY:
						if i in Tile.tilesXY:
							tilesChanged.append(Tile.tilesXY[i])

					tilesChanged.append(colonist.position)
					if abs(visualPosition[0] - currentTile.column*tileSize) >= tileSize or abs(visualPosition[1] - currentTile.row*tileSize) >= tileSize:
						Colonist.moveColonist(colonist, node)
					mark = False
					break
				if colonist.position == node:
					mark = True