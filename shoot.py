from projectile import *
import pygame
from colonist import Colonist
from conf import *
import math
from tile import Tile
import random
from functions import *
from bresenham import getLineOfSight
from structure import Structure

def shoot(colonist):
	# print(colonist.job.status)
	enemy = colonist.job.subject
	weapon = colonist.weapon
	if colonist.job.status == "New":
		if weapon.roundsInMagazine > 0:
			colonist.job.status = "Firing"
		else:
			colonist.job.status = "Reloading"
	if colonist.job.status == "Reloading":
		weapon.reloadState += 1
		# print("reloading " + str(weapon.reloadState))
		if weapon.reloadState >= weapon.reloadCap:
			weapon.roundsInMagazine = weapon.magazineSize
			colonist.job.status = "Firing"
			weapon.reloadState = 0
			conf.sounds['reload'].play()
			return None
	if colonist.job.status == "Firing":
		visible, cover = checkVisibility(colonist, enemy)
		if not visible:
			return None
		if weapon.roundsInMagazine > 0:
			if weapon.rateOfFireState >= weapon.rateOfFire:
				if calculateHit(cover):
					projectile = createProjectile(colonist.visualPosition, enemy.visualPosition, enemy, colonist)
				else:
					surroundingTiles = Tile.getSurroundingTiles(enemy.position)
					randomSurroundingTile = random.choice(surroundingTiles)
					randomSurroundingPosition = Toolbox.tileToVisualPosition(randomSurroundingTile)
					# TODO hit random stuff and make hit less random
					projectile = createProjectile(colonist.visualPosition, randomSurroundingPosition, None, colonist)
				weapon.roundsInMagazine -= 1
				weapon.rateOfFireState = 0
			else:
				weapon.rateOfFireState += 1
		else:
			colonist.job.status = "Reloading"
			weapon.rateOfFireState = weapon.rateOfFire
			weapon.reloadState = 0
	
# return (bool,bool) = (visible, behind cover) 
def checkVisibility(colonist, enemy):
	tilesInLineOfSight = getLineOfSight(colonist.position, enemy.position)
	tiles = []
	for coords in tilesInLineOfSight:
		tile = Tile.getTileByCoords(coords[0], coords[1])
		conf.dotsToDraw.append((tile, 0))
		tiles.append(tile)
	for structure in Structure.structures:
		if structure.position in tiles:
			# check for obstacles except for enemy cover
			distance = Tile.getEuclidianDistance(structure.position, enemy.position) >= 1.5
			if distance >= 1.5:
				# print("cant see")
				return False, False
			else:
				# print("in cover")
				return True, True
				
			# # check if theres empty spots around cover to peek from
			# enemyDirection = getDirection(structure.position, enemy.position)
			# leftTile = Tile.getTileByCoords(math.ceil(structure.position.column - enemyDirection[1]), math.ceil(structure.position.row + enemyDirection[0]) )
			# rightTile = Tile.getTileByCoords(math.ceil(structure.position.column + enemyDirection[1]), math.ceil(structure.position.row - enemyDirection[0]) )
			# tilesToCheck = []
			# if leftTile:
			# 	tilesToCheck.append(leftTile)
			# 	conf.dotsToDraw.append((leftTile, 1))
			# if rightTile:
			# 	tilesToCheck.append(rightTile)
			# 	conf.dotsToDraw.append((rightTile, 2))
			# for tile in tilesToCheck:
			# 	for structure in Structure.structures:
			# 		if structure.position == tile:

			# enemyDirection = getDirection(currentTile, enemy.position)
		# oppositeDirection = (enemyDirection[0] * (- 1), enemyDirection[1] * (- 1))
		# cover = Tile.getTileByCoords(math.ceil(currentTile.column + oppositeDirection[0]), math.ceil(currentTile.row + oppositeDirection[1]) )
	# print ("no cover")
	return True, False

def calculateHit(cover):
	if cover:
		chanceToHit = 20
	else:
		chanceToHit = 100
	hit = random.choice((1,100))
	if hit <= chanceToHit:
		return True
	else:
		return False

def createProjectile(startPos, targetPos, target, owner):
	bullet1 = Bullet(startPos, targetPos, startPos, target, owner, "rocket")
	conf.sounds['shot'].play()
	return bullet1

def moveProjectile(projectile, tileSize):
	start = projectile.currentPos

	end = projectile.targetPos
	vector = (end[0] - start[0], end[1] - start[1])

	vector = pygame.Vector2(vector)
	length = vector.length()
	stepSize = projectile.speed * tileSize

	if stepSize > length:
		stepSize = length
	if length > 0:
		vectorNorm = (vector[0]/length, vector[1]/length)
		projectile.currentPos = (start[0] + vectorNorm[0] * stepSize,start[1] + vectorNorm[1] * stepSize)
	else:
		Projectile.projectiles.remove(projectile)
		hit(projectile)

def hit(projectile):
	targetPos = projectile.targetPos
	# TODO should redraw hit tile
	# tilesChanged.append(targetPos)
	if projectile.target:
		conf.sounds['shot_hit'].play()
		projectile.target.health = projectile.target.health - projectile.damage
	else:
		print("miss")
		conf.sounds['shot_miss'].play()
		

