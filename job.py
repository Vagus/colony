import pygame
import random
from tile import Tile

class Job:
	nJobs = 0
	jobs = []
	def __init__(self, jobType, subject, status = None, position = None, worker = None, team = None, time = None ):
		self.jobType = jobType
		self.subject = subject
		self.status = status
		self.worker = worker
		self.position = position
		self.team = team
		self.time = pygame.time.get_ticks()
		self.nJobs += 1
		self.jobs.append(self)

	def createJob(jobType, subject = None, status = 'new', position = None, worker = None, team=None):
		job1 = Job(jobType, subject, status, position, worker, team)
		return job1

	def delJob(job):
		jobs.remove(job)
		nJobs -= 1
		del job