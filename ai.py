from functions import Toolbox
from tile import Tile
from structure import Structure
import math
from draw import drawText
from conf import Conf
from pygame import Vector2
from shoot import getLineOfSight
from job import Job


def findCover(colonist):
    def sortByDistance(x):
        return x[1]

    enemy = colonist.job.subject
    
    # get closest structure
    tilesWithDistance = []
    for structure in Structure.structures:
        distance = Tile.getEuclidianDistance(colonist.position, structure.position)
        tilesWithDistance.append((structure.position, distance))
    # if no cover on a map
    if len(tilesWithDistance) == 0:
        return False
    tilesWithDistance.sort(key=sortByDistance)
    
        
    Conf.tilesList = tilesWithDistance  # for debug purposes

    # for _tile in Tile.tiles:
    #     if _tile.colonistStanding != None:
    #         print(_tile)
    # get potential cover tiles with a distance
    for structureTile, _ in tilesWithDistance:
        surroundingTiles = Tile.getSurroundingTiles(structureTile)
        coverWithDistance = []
        for _tile in surroundingTiles:
            # check there is no1 on tile
            if _tile.colonistStanding == None:
                distance = Tile.getEuclidianDistance(colonist.position, _tile)
                coverWithDistance.append((_tile, distance))
        coverWithDistance.sort(key=sortByDistance)

        # prepare claimed job positions
        jobPositions = []
        for job in Job.jobs:
            if not job.position == None:
                # do not count colonists job position
                if not colonist.job.position:
                    jobPositions.append(job.position)

        # calculate angle between cover and enemy
        for cover_tile in coverWithDistance:
            # filter out tiles already claimed by jobs
            if cover_tile[0] in jobPositions:
                # print("filtered")
                continue
            Conf.dotsToDraw.append((cover_tile[0], 1))
            directionToEnemy = Toolbox.getDirection(cover_tile[0], enemy.position)
            directionToCover = Toolbox.getDirection(cover_tile[0], structureTile)
            dot = Vector2.dot(directionToEnemy, directionToCover)
            angle = math.degrees(math.acos(dot))

            if not angle < Conf.cover_angle:
                continue
            # distance = tilesList[0][1]
            # check that is closer than enemy
            # if distance < Tile.getEuclidianDistance(colonist.position, enemy.position):
            # enemyDirection = getDirection(currentTile, enemy.position)
            # oppositeDirection = (enemyDirection[0] * (- 1), enemyDirection[1] * (- 1))
            # cover = Tile.getTileByCoords(math.ceil(currentTile.column + oppositeDirection[0]), math.ceil(currentTile.row + oppositeDirection[1]) )

            
            # check if cover is reachable
            if Tile.isReachable(colonist.position, cover_tile[0]):
                Conf.dotsToDraw.append((cover_tile[0], 2))
                # print('cover tile ' + str(cover_tile[0].tileNumber) + ' equals ' + str(colonist.)
                return cover_tile[0]
    return False

def validateCover(colonist, coverTile):
    # check there is no1 on tile
    if colonist.position == coverTile or Tile.isReachable(colonist.position, coverTile):
        # check if cover is between colonist and enemy
        directionToEnemy = Toolbox.getDirection(coverTile, colonist.enemy.position)
        surroundingTiles = Tile.getSurroundingTiles(coverTile)
        # check if cover provides cover from enemy
        for _tile in surroundingTiles:
            if not _tile.structure == None:
                directionToCover = Toolbox.getDirection(coverTile, _tile)
                dot = Vector2.dot(directionToEnemy, directionToCover)
                angle = math.degrees(math.acos(dot))
                if angle < Conf.cover_angle:
                    return True

def enemyInRange(colonist):
    distance = Tile.getEuclidianDistance(
        colonist.position, colonist.job.subject.position
    )
    if distance <= colonist.weapon.weaponRange:
        return True
    else:
        return False