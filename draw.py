from tile import Tile
from conf import conf
import math
from button import Button
from colonist import Colonist
from functions import *
from projectile import *
from structure import Structure
import pygame.math
import spritesheet
from UI import *

screen = None
# update screen with new tiles
def redrawRects(screen, images, flipMark, clock, mousePosition):
    screen = screen
    tileSize = Tile.tileSize
    rects = []
    for tile in tilesChanged:
        rects.append(reDrawTile(tile, tileSize, screen, images))
    tilesChanged.clear()
    for dot, color in conf.dotsToDraw:
        rects.append(drawDot(dot, color, tileSize, screen))
    conf.dotsToDraw.clear()
    for path in activePaths:
        rects.extend((drawPath(path, tileSize, screen)))
    for colonist in Colonist.colonists:
        rects.append(drawColonist(colonist, screen, tileSize, images))
    for structure in Structure.structures:
        rects.append(drawStructure(structure, screen, tileSize, images))
    for projectile in Projectile.projectiles:
        rects.append(drawProjectile(projectile, images, tileSize, screen))
    if conf.selectedThing == "buildWall":
        rects.append(drawBuilding(screen, tileSize, images, mousePosition))
    rects.extend(drawUI(screen))
    rects.append(drawFPS(clock, screen))
    if hasattr(conf, "tilesList"):
        rects.extend(drawText(conf.tilesList))
    # print (rects)
    if flipMark:
        # print("flip")
        pygame.display.flip()
        flipMark = False
    else:
        pygame.display.update(rects)
    return flipMark

def drawBuilding(screen, tileSize, images, mousePosition):
    image = images["woodenWall"]
    alpha = 100
    surf = pygame.Surface((tileSize, tileSize))
    # image = pygame.transform.scale(image, (tileSize, tileSize))
    surf.blit(image, (0, 0))
    # surf.fill((0, 255, 0, 90))
    surf.set_alpha(alpha)
    # surf.fill((255,255,255, alpha), None, pygame.BLEND_RGBA_MULT)
    # surf.blit(image, (0, 0), special_flags=pygame.BLEND_RGBA_MULT)
    x,y = mousePosition

    tileCoords = ((x  + Tile.relCornerX) // tileSize, (y + Tile.relCornerY) // tileSize)
    # print(tileCoords)
    if tileCoords in Tile.tilesXY:
        cursorTile = Tile.tilesXY[tileCoords]
        conf.dotsToDraw.append((cursorTile, 2))
        tilesChanged.append(cursorTile)

    xRel = (x) // tileSize
    yRel = (y) // tileSize
    if (xRel,yRel) in Tile.tilesXY:
        tile = Tile.tilesXY[xRel,yRel]
        # conf.dotsToDraw.append((tile, 2)) 
        return screen.blit(surf,((tile.column) * tileSize, (tile.row) * tileSize))

def redrawWindow(screen, images):
    tileSize = Tile.tileSize
    count = 0
    screen.fill((0,0,0))
    worldWidth = conf.columns * Tile.tileSize
    worldHeight = conf.rows * Tile.tileSize
    _screenHeight = SCREEN_HEIGHT - PANEL_HEIGHT
    tilesChanged.clear()
    topLeftX = Tile.relCornerX // tileSize
    topLeftY = Tile.relCornerY // tileSize
    bottomRightX = worldHeight // tileSize
    bottomRightY = worldWidth // tileSize

    if topLeftX < 0:
        topLeftX = 0
        bottomRightX = (SCREEN_WIDTH - abs(Tile.relCornerX)) // tileSize + 1
    if topLeftY < 0:
        topLeftY = 0
        bottomRightY = (_screenHeight - abs(Tile.relCornerY)) // tileSize + 1
    if bottomRightX >= worldWidth - abs(Tile.relCornerX):
        topLeftX = (worldWidth - abs(Tile.relCornerX)) // tileSize
    if bottomRightY >= worldHeight - abs(Tile.relCornerY):
        topLeftY = (worldHeight - abs(Tile.relCornerY)) // tileSize
    # print(str(bottomRightX) + " " + str(bottomRightY))
    for i in range(bottomRightX):
        # print(x)
        y = topLeftY
        for b in range(bottomRightY):
            if (topLeftX, y) in Tile.tilesXY:
                # print(str(topLeftX) + " " + str(bottomRightX))
                tile = Tile.tilesXY[topLeftX,y]
                tilesChanged.append(tile)
                # dotsToDraw.append(tile)
                count += 1
            y +=1
        topLeftX += 1

    # print(count)

def reDrawTile(tile, tileSize, screen, images):
    content = tile.content
    tileType = tile.tileType
    tileImage = images[tileTypes[tileType]["image"]]
    surf = pygame.Surface((tileSize, tileSize))
    rect = surf.get_rect()
    x = surf.blit(tileImage, rect)
    # if content:
    #     item = content.item
    #     if type(item) == str:
    #         item = images[item]
    #     x = surf.blit(item, rect)
    return screen.blit(surf ,(tile.column * tileSize - Tile.relCornerX, tile.row * tileSize - Tile.relCornerY))

def drawColonist(colonist, screen, tileSize, images):
    image = images[colonist.image]
    image = pygame.transform.scale(image, (tileSize, tileSize))
    if conf.selectedThing == colonist:
        marker = images['selected']
        marker = pygame.transform.scale(marker, (tileSize, tileSize))
        image.blit(marker,(0,0))
    if colonist.weapon:
        weaponIcon = images[colonist.weapon.image]
        weaponIcon = pygame.transform.scale(weaponIcon, (tileSize//2, tileSize//2))
        image.blit(weaponIcon, (0,tileSize//2))
    return screen.blit(image,(colonist.visualPosition[0] - Tile.relCornerX, colonist.visualPosition[1] - Tile.relCornerY))

def drawPathValues(pathTiles, screen, tileSize):
    rects = []
    for tile in pathTiles:
        tilesChanged.append(tile)
        text_surface, rect = GAME_FONT.render(str(math.floor(tile.f)))
        rects.append(screen.blit(text_surface, (tile.column * tileSize - Tile.relCornerX, tile.row * tileSize - Tile.relCornerY)))
    return rects

def drawText(tilesWithValues):
    if tilesWithValues:
        tileSize = Tile.tileSize
        screen = conf.screen
        rects = []
        for tile, text in tilesWithValues:
            round(text)
            tilesChanged.append(tile)
            text_surface, rect = GAME_FONT.render(str(text))
            rects.append(screen.blit(text_surface, (tile.column * tileSize - Tile.relCornerX + 5, tile.row * tileSize - Tile.relCornerY + 5)))
        return rects

def drawFPS(clock, screen):
    surf = pygame.Surface((50, 60))
    rect = surf.get_rect()
    FPS_FONT = pygame.freetype.SysFont('Comic Sans MS', 20)
    tickTime, ab = FPS_FONT.render(str(math.floor(clock.get_time())), (255,100,255))
    fps, ab = FPS_FONT.render(str(math.floor(clock.get_fps())), (255,100,255))
    rects = []
    surf.blit(tickTime, (0, 0))
    surf.blit(fps, (0, 30))

    # tilesChanged.extend(surf)
    return screen.blit(surf, (SCREEN_WIDTH - 60, 0))

def drawStructure(structure, screen, tileSize, images):
    surf = pygame.Surface((tileSize, tileSize))
    if not structure.completed:
        image = images["tileBlueprint"]
        # ss = spritesheet.spritesheet('Wall_Blueprint_Atlas.png')
        # Sprite is 16x16 pixels at location 0,0 in the file...

        # cuts = []
        # Load two images into an array, their transparent bit is (255, 255, 255)
        # cuts = ss.images_at((0, 0, 16, 16),(17, 0, 16,16), colorkey=(255, 255, 255))
        # surf.fill((0, 255, 0, 90))
        surf.set_alpha(100)
        # image.fill((255,255,255, alpha), None, pygame.BLEND_RGBA_MULT)
        # image.blit(surf, (0, 0), special_flags=pygame.BLEND_RGBA_MULT)
    else:
        image = images[structure.image]
    surf.blit(image, (0, 0))
    tile = structure.position
    # image = pygame.transform.scale(image, (tileSize, tileSize))
    return screen.blit(surf,(tile.column * tileSize - Tile.relCornerX, tile.row * tileSize - Tile.relCornerY))

def drawPath(path, tileSize, screen):
    rects = []
    path = activePaths[path]
    if path:
        currentStep = path[0]
        startPos = ((currentStep.column * tileSize) + tileSize//2 - Tile.relCornerX, currentStep.row * tileSize + tileSize//2 - Tile.relCornerY)
        for step in path:
            endPos = ((step.column * tileSize) + tileSize//2 - Tile.relCornerX, (step.row * tileSize) + tileSize//2 - Tile.relCornerY)
            rects.append(pygame.draw.line(screen, (255,255,255), startPos, endPos, width=1))
            startPos = endPos
            currentStep = step
            tilesChanged.append(step)
    return rects

def drawDot(tile, color, tileSize, screen):
    if tile:
        colors = [(255, 0, 0), (0, 255, 0),(0, 0, 255)]
        chosenColor = colors[color]
        rects = []
        tileSize = Tile.tileSize
        rect = pygame.draw.circle(screen, chosenColor, ((tile.column * tileSize + tileSize//2 + color)  - Tile.relCornerX, (tile.row * tileSize + tileSize//2 + color)  - Tile.relCornerY), (tileSize//3))
        return rect

def drawUI(screen):
    rects = []
    panelSize = (SCREEN_WIDTH, PANEL_HEIGHT)
    panelSurf = pygame.Surface(panelSize)
    panelRect = panelSurf.get_rect()
    panelSurf.fill((255,255,255))
    rects.append(screen.blit(panelSurf, (0, SCREEN_HEIGHT - panelSize[1])))
    getButtonText()
    for button in Button.buttons:
        outlineWidth = 4
        buttonOutline = pygame.Surface(button.size)
        buttonOutline.fill((47,79,79))
        buttonSurf = pygame.Surface((button.size[0] - (outlineWidth * 2), button.size[1] - outlineWidth))
        buttonSurf.fill((105,105,105))
        rect = buttonSurf.get_rect()
        text_surface, rect = GAME_FONT.render(button.text)
        rect = text_surface.get_rect()
        buttonSurf.blit(text_surface, (button.size[0]/2 - rect.width/2, button.size[1]/2 - rect.height/2))
        buttonOutline.blit(buttonSurf, (outlineWidth, outlineWidth))
        rects.append(screen.blit(buttonOutline, button.position))
    return rects

def zoomCamera(increment, surface, images):    
    tileSize = Tile.tileSize + (10 * increment)
    if tileSize < 20:
        tileSize = 20
    if tileSize > 90:
        tileSize = 90
    Tile.tileSize = tileSize
    redrawWindow(surface, images)
    flipMark = True
    return surface, flipMark

def moveScreen(direction, surface, images):
    tileSize = Tile.tileSize
    step = tileSize
    limit = 300
    worldWidth = conf.columns * tileSize
    worldHeight = conf.rows * tileSize
    _screenHeight = SCREEN_HEIGHT - PANEL_HEIGHT
    if direction == "right":
        if not Tile.relCornerX < -limit:
            Tile.relCornerX = Tile.relCornerX - step
    elif direction == "left":
        if not Tile.relCornerX > limit + (worldWidth - Tile.relCornerX):
        # if not SCREEN_WIDTH - worldWidth - abs(Tile.relCornerX) > limit:
            Tile.relCornerX = Tile.relCornerX + step
    elif direction == "up":
            if not Tile.relCornerY > limit + (worldHeight + PANEL_HEIGHT - Tile.relCornerY):
                Tile.relCornerY = Tile.relCornerY + step
    elif direction == "down":
        if not Tile.relCornerY < -limit:
            Tile.relCornerY = Tile.relCornerY - step
    redrawWindow(surface, images)
    return True


def drawProjectile(projectile, images, tileSize, screen):
    start = projectile.currentPos[0] - Tile.relCornerX, projectile.currentPos[1] - Tile.relCornerY,
    # if target is a tile
    if isinstance(projectile.targetPos, Tile):
        end = (projectile.targetPos.column, projectile.targetPos.row)
        vector = (projectile.targetPos.column - Tile.relCornerX - start[0], projectile.targetPos.row - Tile.relCornerY - start[1])
    # if target is colonist
    else:
        end = projectile.targetPos
        vector = (end[0] - Tile.relCornerX - start[0], end[1] - Tile.relCornerY - start[1])

    if not start == end:
        vector = pygame.Vector2(vector)
        distance = Toolbox.vectorLength(vector)
        _, angle = vector.as_polar()

        image = images[projectile.image]
        image = pygame.transform.rotate(image, int(-angle - 90))
        image = pygame.transform.scale(image, (tileSize, tileSize))
        tile = Tile.getTileByVisualPosition((projectile.currentPos[0], projectile.currentPos[1]))
        # print(str(tile.column) + " " + str(tile.row))
        if tile:
            tilesChanged.append(tile)
            # conf.dotsToDraw.append((tile, 1))
            surroundingTiles = Tile.getSurroundingTiles(tile)
            for tile in surroundingTiles:
                tilesChanged.append(tile)
                # conf.dotsToDraw.append((tile, 2))
        # for tile in Tile.tiles:
        #     if tile.column == projectile.currentPos[0] - Tile.relCornerX // tileSize and tile.row == projectile.currentPos[1] - Tile.relCornerY // tileSize:
        #         tilesChanged.append(tile)
        #     if tile.column <= projectile.currentPos[0] - Tile.relCornerX // tileSize + 1 and tile.column >= projectile.currentPos[0] - Tile.relCornerX // tileSize - 1:
        #         if tile.row <= projectile.currentPos[1] - Tile.relCornerY // tileSize + 1 and tile.row >= projectile.currentPos[1] - Tile.relCornerY // tileSize - 1:
        #             tilesChanged.append(tile)
        if distance > tileSize/2:
            return screen.blit(image, start)
        else:
            return None
    else:
        return None


