import math
# A* search
def astar_search(tiles, startTile, endTile, tileTypes):
    # Create lists for open nodes and closed nodes
    openList = {}
    closedList = {}
    path = []

    if tiles[endTile]['type'] == "water":
        print("water unreachable")
        return None, None

    for tile in tiles:
        tiles[tile]['f'] = tiles[tile]['g'] = tiles[tile]['h'] = tiles[tile]['parent'] = 0

    # Add the start node
    openList[startTile] = startTile
    # tiles[startTile]['f'] = 99
    currentNode = startTile
    # Loop until the open list is empty
    while len(openList) > 0:
        # Sort the open list to get the node with the lowest cost first
        fList = []
        for key in openList:
            fList.append(tiles[key]['f'])
        fList.sort()

        lowest = fList[0]
        # Get the node with the lowest cost

        for key in openList:
            if tiles[key]['f'] == lowest:
                
                currentNode = key

        openList.pop(currentNode, None)
        # Add the current node to the closed list
        closedList[currentNode] = currentNode
        # Check if we have reached the goal, return the path
        # 
        if currentNode == endTile:
            # print("found it")
            path = []
            path.append(currentNode)
            while currentNode != startTile:
                # print ("current " + str(currentNode) + " end tile " + str(endTile))
                # print(str(startTile) + str(tiles[currentNode]['parent']))
                # print("tile " + str(currentNode) + " with parent " + str(tiles[currentNode]['parent']))
                currentNode = tiles[currentNode]['parent']
                path.append(currentNode)

            # Return reversed path
            # print (path[::-1])
            return closedList, path[::-1]

        neighbors = []
        # Select 8 nodes around currentNode
        Tile.tilesXY
        for tile in tiles:
            # print (tileTypes[tiles[tile]['type']]['cost'])
            if tileTypes[tiles[tile]['type']]['cost'] < 100:
                if not tile in closedList and not tile in openList:
                    if tiles[tile]['row'] <= tiles[currentNode]['row'] + 1 and tiles[tile]['row'] >= tiles[currentNode]['row'] - 1:
                        if tiles[tile]['column'] <= tiles[currentNode]['column'] + 1 and tiles[tile]['column'] >= tiles[currentNode]['column'] - 1:
                            neighbors.append(tile)
                            tiles[tile]['parent'] = currentNode
        # print(len(neighbors))
        # Loop neighbors
        for i in neighbors:
            cost = tileTypes[tiles[i]['type']]['cost']
            # print(cost)
            tiles[i]['g'] = math.sqrt((tiles[i]['row'] - tiles[startTile]['row']) ** 2 + (tiles[i]['column'] - tiles[startTile]['column']) ** 2) + cost
            tiles[i]['h'] = math.sqrt((tiles[i]['row'] - tiles[endTile]['row']) ** 2 + (tiles[i]['column'] - tiles[endTile]['column']) ** 2) + cost
            # if tiles[next] == endTile:
                # print ("current" + str(currentNode) + " with " + str(tiles[currentNode]['f']) + " <= than " + str(tiles[next]['f']))
            tiles[i]['f'] = tiles[i]['g'] + tiles[i]['h']
            # Check if neighbor is in open list and if it has a lower f value
            # print ("current" + str(currentNode) + " with " + str(tiles[currentNode]['f']) + " <= than " + str(tiles[next]['f']))
            # if tiles[next]['f'] <= tiles[currentNode]['f']:
            openList[i] = i
        # print(tiles)
        # print(openList)
        # print(startTile)
        # print (closedList)
        # openList.pop(currentNode, None)
    # Return None, no path is found
    return closedList, path