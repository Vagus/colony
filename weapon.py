class Weapon:
	nWeapons = 0
	weapons = []

	def __init__(self, name, image, rateOfFire, rateOfFireState, magazineSize, roundsInMagazine, reloadCap, reloadState, weaponRange):
		self.name = name
		self.image = image
		self.reloadCap = reloadCap
		self.reloadState = reloadState
		self.rateOfFire = rateOfFire
		self.rateOfFireState = rateOfFireState
		self.magazineSize = magazineSize
		self.roundsInMagazine = roundsInMagazine
		self.weaponRange = weaponRange
		Weapon.nWeapons += 1
		# damage is property of projectiles
		self.weapons.append(self)

class Rocket(Weapon):
	def __init__(self, name = "Rocket", image = "rocket", projectile = "rocket", rateOfFire = 50, rateOfFireState = 50, magazineSize = 5, roundsInMagazine = 5, 
		reloadCap = 50, reloadState = 0, weaponRange = 50):
		super().__init__(name, image, rateOfFire, rateOfFireState, magazineSize, roundsInMagazine, reloadCap, reloadState, weaponRange)

class Gauss(Weapon):
	def __init__(self, name = "Gauss", image = "gauss_rifle", projectile = "rocket", rateOfFire = 50, rateOfFireState = 50, magazineSize = 5, roundsInMagazine = 5, 
		reloadCap = 50, reloadState = 0, weaponRange = 3):
		super().__init__(name, image, rateOfFire, rateOfFireState, magazineSize, roundsInMagazine, reloadCap, reloadState,  weaponRange)

class Sniper(Weapon):
	def __init__(self, name = "Sniper", image = "sniper_rifle", projectile = "rocket", rateOfFire = 50, rateOfFireState = 50, magazineSize = 5, roundsInMagazine = 5, 
		reloadCap = 50, reloadState = 0, weaponRange = 15):
		super().__init__(name, image, rateOfFire, rateOfFireState, magazineSize, roundsInMagazine, reloadCap, reloadState, weaponRange)