import math
from conf import conf
import pygame
from tile import *

class Toolbox:

	def vectorLength(a):
		return math.sqrt(a[0] * a[0] + a[1] * a[1])
	
	def tileToVisualPosition(tile):
		return tile.column * Tile.tileSize, tile.row * Tile.tileSize
	
	def getDirection(startTile, endTile):
		if startTile == endTile:
			raise ValueError('same tiles')
		vector = (endTile.column - startTile.column, endTile.row - startTile.row)
		vector = pygame.Vector2(vector)
		return pygame.Vector2.normalize(vector)

	def get_tiles_in_angle_cone(original_tile, direction_vector, cone_angle, distance=1, num_steps=4):
		"""
		original_tile: Tuple (x, y) representing the coordinates of the original tile.
		direction_vector: Tuple (dx, dy) representing the normalized vector direction.
		cone_angle: The angle of the cone in degrees.
		distance: The distance to the neighboring tiles (default is 1).
		num_steps: The number of steps to iterate over within the cone (default is 360).
		Returns: List of tuples representing the coordinates of neighboring tiles within the angle cone.
		"""
		column = original_tile.column
		row = original_tile.row
		dx, dy = direction_vector

		# Calculate the starting angle
		start_angle = math.atan2(dy, dx)
		
		# Calculate the angles within the cone
		angles = [start_angle + math.radians(i) for i in range(-cone_angle // 2, cone_angle // 2 + 1)]

		# Calculate the neighboring tile coordinates for each angle
		neighboring_tiles_coords = [(column + distance * math.cos(angle), row + distance * math.sin(angle)) for angle in angles]

		# Round the coordinates to integers if needed
		neighboring_tiles_coords = [(int(tile[0]), int(tile[1])) for tile in neighboring_tiles_coords]
		
		neighboring_tiles = []
		for tile in neighboring_tiles_coords:
			neighboring_tiles.append(getTileByCoords(tile))
		
		return neighboring_tiles

		# # Example usage:
		# original_tile = (2, 3)
		# direction_vector = (1, 1)  # Example: diagonal vector
		# cone_angle = 45  # Example: 45 degrees cone angle

		# neighboring_tiles = get_tiles_in_angle_cone(original_tile, direction_vector, cone_angle)
		# print("Original Tile:", original_tile)
		# print("Direction Vector:", direction_vector)
		# print("Cone Angle (degrees):", cone_angle)
		# print("Neighboring Tiles within Cone:", neighboring_tiles)

# def visualPositionToTile(pos):
# 	return Tile.tiles