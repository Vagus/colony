import pygame
from pygame.locals import *
import pygame.freetype

SCREEN_WIDTH = 1024
SCREEN_HEIGHT = 768
PANEL_HEIGHT = 100
FPS = 25
extraColonists = 1
volume = 0.5

tilesChanged = []
activePaths = {}

colonistsToMove = []
pygame.init()
clock = pygame.time.Clock()
GAME_FONT = pygame.freetype.SysFont('Comic Sans MS', 15)


class Conf:
    rows = 10
    columns = 10
    cover_angle = 40
    number_of_structures = 10
    selectedThing = None
    dotsToDraw = []
    paused = False
    screen = None
    def __init__(self):
        pass

    def loadSounds():
        sounds = {
            'shot': "shot.wav",
            'shot_hit': "shot_hit.mp3",
            'shot_miss': "shot_miss.wav",
            'reload': "reload.wav"
        }
        for k,v in sounds.items():
            v = "fx/" + v
            pygame.mixer.init()
            sounds[k] = pygame.mixer.Sound(v)
        return sounds
    sounds = loadSounds()

    def setVolume(sounds):
        for _, sound in sounds.items():
            sound.set_volume(volume)

    setVolume(sounds)


tileTypes = {
    "dirt": {
        'image': 'dirtTile',
        'cost': 5
        },
    # 'water': {
    #      "image": 'waterTile',
    #      'cost': 100
    #     },
    'wood': {
        "image": 'woodTile',
         'cost': 1
        }
    }

def loadImages(tileSize):
    images = {
        'dirtTile': "Soil.png",
        'waterTile': "WaterDeep.png",
        'woodTile':"WoodFloor.png",
        'chicken': "Chicken_side.png",
        'woodenWall': "wall.png",
        'capybara': "Capybara_front.png",
        'rocket': "Rocket_Big.png",
        'chick_big': "chick_bigger.png",
        'piggy': "piggy.png",
        'alpaca': "alpaca.png",
        'bull': "bull.png",
        'sexy-cow': "sexy-cow.png",
        'pig': "pig.png",
        'sheep': "sheep.png",
        'fox': "fox.png",
        'selected':'ReservedForWork.png',
        'blueprintWall': "Wall_Blueprint_Atlas.png",
        'dead': "dead.png",
        'tileBlueprint': "Tile.png",
        'icon_gauss': "icon_gauss.png",
        'icon_sniper': "icon_sniper.png",
        'sniper_rifle': "sniper_rifle.png",
        'gauss_rifle': "gauss_rifle.png"
    }
    for k,v in images.items():
        v = "images/" + v
        images[k] = pygame.image.load(v).convert_alpha()
        # images[k] = pygame.transform.scale(images[k],(tileSize, tileSize))
    return images


conf = Conf()