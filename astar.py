import math
from tile import Tile
from conf import tileTypes

def getF(_tile):
    return _tile.f

# A* search
def astar_search(_startTile, _endTile):
    # Create lists for open nodes and closed nodes
    _openList = []
    _closedList = []
    if _endTile.tileType == "water":
        return None, None

    # Add the start node
    _startTile.f = _startTile.g = 0
    _openList.append(_startTile)
    _currentNode = _startTile

    # Loop until the open list is empty
    while len(_openList) > 0:

        _currentNode = min(_openList, key=getF)
        _openList.remove(_currentNode)
        # Add the current node to the closed list
        _closedList.append(_currentNode)

        # Check if we have reached the goal, return the path
        if _currentNode == _endTile:
            # print("found it")
            _path = []
            _path.append(_currentNode)
            while _currentNode != _startTile:
                _currentNode = _currentNode.parent
                _path.append(_currentNode)
            # Return reversed path
            return _closedList, _path[::-1]

        _neighborXY = [(_currentNode.column + 1, _currentNode.row + 1), (_currentNode.column + 1, _currentNode.row), (_currentNode.column + 1, _currentNode.row - 1),
                    (_currentNode.column, _currentNode.row + 1),(_currentNode.column, _currentNode.row - 1),
                    (_currentNode.column - 1, _currentNode.row + 1), (_currentNode.column - 1, _currentNode.row), (_currentNode.column - 1, _currentNode.row -1)]
        # Select 8 nodes around currentNode
        for i in _neighborXY:
            if i in Tile.tilesXY:
                _tile = Tile.tilesXY[i]
                if _tile.structure and _tile.structure.completed:
                    _cost = _tile.structure.cost
                else:
                    _cost = tileTypes[_tile.tileType]['cost']
                if _cost < 100:
                    if not _tile in _closedList and not _tile in _openList: 
                        _tile.parent = _currentNode
                        _tile.g = _cost + _tile.parent.g
                        _tile.h = math.sqrt((_tile.column - _endTile.column) ** 2 + (_tile.row - _endTile.row) ** 2)
                        _tile.f = _tile.g + _tile.h
                        _openList.append(_tile)

    # print ("no path")
    return None, None