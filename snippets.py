# black on the left
    if Tile.relCornerX < 0:
        print("black on the left")
        x = 0
        xRange = (SCREEN_WIDTH - abs(Tile.relCornerX)) // tileSize

    # black on the right
    elif worldWidth - abs(Tile.relCornerX) < SCREEN_WIDTH:
        print("black on the right")
        x = Tile.relCornerX // tileSize 
        xRange = (worldWidth - abs(Tile.relCornerX)) // tileSize

    # black on left and right
    if Tile.relCornerX < 0 and SCREEN_WIDTH - abs(Tile.relCornerX) > worldWidth:
        print("black on left and right")
        x = 0
        xRange = worldWidth // tileSize

        # no black on left or right
    if Tile.relCornerX >= 0 and SCREEN_WIDTH + abs(Tile.relCornerX) < worldWidth:
        print("no black on left and right")
        x = Tile.relCornerX // tileSize
        xRange = SCREEN_WIDTH // tileSize
    
    # black on top
    if Tile.relCornerY < 0:
        print("black on top")
        y = 0
        yRange = (_screenHeight - abs(Tile.relCornerY)) // tileSize
    # black on bottom
    if SCREEN_HEIGHT - worldHeight - abs(Tile.relCornerY) > 0:
        print("black on bottom")
        y = Tile.relCornerY // tileSize
        yRange = (worldHeight - abs(Tile.relCornerY)) // tileSize
    #  black on top and bottom
    if Tile.relCornerY < 0 and _screenHeight - abs(Tile.relCornerY) > worldHeight:
        print("black on top and bottom")
        yRange = worldHeight // tileSize
    # no black on top or bottom
    if Tile.relCornerY >= 0 and _screenHeight < worldHeight:
        print("no black on top or bottom")
        y = Tile.relCornerY // tileSize
        yRange = _screenHeight // tileSize

# Define a Player object by extending pygame.sprite.Sprite
# The surface drawn on the screen is now an attribute of 'player'
class Player(pygame.sprite.Sprite):
    def __init__(self):
        super(Player, self).__init__()
        self.surf = pygame.Surface((75, 25))
        self.surf.fill((255, 255, 255))
        self.surf = pygame.image.load("Naked_Female_front.png").convert()
        self.surf.set_colorkey((255, 255, 255), RLEACCEL)
        self.rect = self.surf.get_rect()

# Define the enemy object by extending pygame.sprite.Sprite
# The surface you draw on the screen is now an attribute of 'enemy'
class Enemy(pygame.sprite.Sprite):
    def __init__(self):
        super(Enemy, self).__init__()
        self.surf = pygame.image.load("Capybara_front.png").convert()
        self.surf.set_colorkey((255, 255, 255), RLEACCEL)
        self.rect = self.surf.get_rect(
            center=(
                random.randint(SCREEN_WIDTH + 20, SCREEN_WIDTH + 100),
                random.randint(0, SCREEN_HEIGHT),
            )
        )
        self.speed = random.randint(5, 20)

    # Move the sprite based on speed
    # Remove the sprite when it passes the left edge of the screen
    def update(self):
        self.rect.move_ip(-self.speed, 0)
        if self.rect.right < 0:
            self.kill()

def drawGrid(rows, columns, surface):
    pass
    # sizeBtwn = SCREEN_HEIGHT // columns

    # x = 0
    # y = 0
    # for l in range(rows):
    #     x = x + sizeBtwn
    #     y = y + sizeBtwn
 
    #     pygame.draw.line(surface, (255,255,255), (x,0),(x,SCREEN_WIDTH))
    #     pygame.draw.line(surface, (255,255,255), (0,y),(SCREEN_HEIGHT,y))

    # Create the 'player'

    # Move the sprite based on user keypresses
def update(self, pressed_keys):
    if pressed_keys[K_UP]:
        self.rect.move_ip(0, -5)
    if pressed_keys[K_DOWN]:
        self.rect.move_ip(0, 5)
    if pressed_keys[K_LEFT]:
        self.rect.move_ip(-5, 0)
    if pressed_keys[K_RIGHT]:
        self.rect.move_ip(5, 0)

    # Keep player on the screen
    if self.rect.left < 0:
        self.rect.left = 0
    if self.rect.right > SCREEN_WIDTH:
        self.rect.right = SCREEN_WIDTH
    if self.rect.top <= 0:
        self.rect.top = 0
    if self.rect.bottom >= SCREEN_HEIGHT:
        self.rect.bottom = SCREEN_HEIGHT
        
player = Player()

# Create a custom event for adding a new enemy
ADDENEMY = pygame.USEREVENT + 1
pygame.time.set_timer(ADDENEMY, 250)

# Create groups to hold enemy sprites and all sprites
# - enemies is used for collision detection and position updates
# - all_sprites is used for rendering
enemies = pygame.sprite.Group()
all_sprites = pygame.sprite.Group()
all_sprites.add(player)

import pygame.freetype
import math
def findPath(tiles, startTile, endTile, tileTypes):
    # path = []
    # for tile in tiles:
    #   tiles[tile]['f'] = tiles[tile]['g'] = tiles[tile]['h'] = 0
    #   cost = tileTypes[tiles[tile]['type']]['cost']
    #   tiles[tile]['g'] = abs(tiles[tile]['row'] - tiles[startTile]['row']) + abs(tiles[tile]['column'] - tiles[endTile]['column'])
    #   # math.sqrt((tiles[tile]['row'] - tiles[startTile]['row']) ** 2 + (tiles[tile]['column'] - tiles[startTile]['column']) ** 2)
    #   tiles[tile]['h'] = math.sqrt((tiles[tile]['row'] - tiles[endTile]['row']) ** 2 + (tiles[tile]['column'] - tiles[endTile]['column']) ** 2)
    #   tiles[tile]['f'] = tiles[tile]['g'] + tiles[tile]['h']
    openList = []
    closedList = []

    openList.append(startTile)
    currentNode = startTile
    # print (openList)
    index = 0
    while len(openList) > 0 and index < 50:
        # print (currentNode)
        index = index + 1
        for tile in tiles:
            if not tile == currentNode:
                if tileTypes[tiles[tile]['type']]['cost'] < 100:
                    if not tile in openList:
                        if tiles[tile]['row'] <= tiles[currentNode]['row'] + 1 and tiles[tile]['row'] >= tiles[currentNode]['row'] - 1:
                            if tiles[tile]['column'] <= tiles[currentNode]['column'] + 1 and tiles[tile]['column'] >= tiles[currentNode]['column'] - 1:
                                openList[tile] = tiles[tile]
        print(openList.keys())

        openList.pop(currentNode, None)
        closedList[currentNode] = tiles[currentNode]

        if currentNode == startTile:
            currentNode = next(iter(openList))
        previousNode = currentNode
        for tile in openList:
            tiles[tile]['g'] = abs(tiles[tile]['row'] - tiles[startTile]['row']) + abs(tiles[tile]['column'] - tiles[endTile]['column'])
            tiles[tile]['h'] = math.sqrt((tiles[tile]['row'] - tiles[endTile]['row']) ** 2 + (tiles[tile]['column'] - tiles[endTile]['column']) ** 2)
            tiles[tile]['f'] = tiles[tile]['h'] + tiles[tile]['g']
            print ("tile " + str(tile) + " value " + str(openList[tile]['f']) + " less than tile " + str(currentNode) + " with " + str(tiles[currentNode]['f']))
            if openList[tile]['f'] < tiles[currentNode]['f'] or openList[tile]['f'] == tiles[currentNode]['f'] and openList[tile]['h'] < tiles[currentNode]['h']:
                currentNode = tile
        if currentNode == previousNode:
            print("nasrat")
        # print ("tile selected " + str(currentNode))
        
        if currentNode == endTile:
            openList = {}
            print("found it")
            
            for tile in closedList:
                path.append(tile)
            print(len(path))
            path = path[::-1]
            return (closedList, path)
        elif len(openList) == 0:
            print("destination unreachable")

        # path.append(currentNode)
        

        # print(str(currentNode) + " " + str(endTile))

        

    return closedList, path
# TODO tile muze byt nedostupna




