from button import Button
from conf import *
from build import *
from tile import Tile
from colonist import Colonist

def makeUIElements():
    button1 = Button("Build", (150, 50),  (0, 1))
    button2 = Button("Work", (150, 50),  (1, 1))
    button3 = Button("Colonist panel", (1000, 50),  (0, 2))
    button4 = Button("Save", (150, 50),  (2, 1))
    for button in Button.buttons:
        button.position = (button.position[0] * button.size[0], SCREEN_HEIGHT - button.position[1] * button.size[1])

def detectClick(mousePosition, button):
    x,y = mousePosition
    x = x + Tile.relCornerX
    y = y + Tile.relCornerY

    if x < 0 or y < 0:
        # print ("out of bounds")
        if button == 3:
            conf.selectedThing = None
    else:
        worldWidth = conf.columns * Tile.tileSize
        worldHeight = conf.rows * Tile.tileSize
        
        # clicked on panel
        if mousePosition[1] > SCREEN_HEIGHT - PANEL_HEIGHT:
            if mousePosition[1] > SCREEN_HEIGHT - 50 and mousePosition[0] <= 150 and mousePosition[0] >= 0:
                build()
                print("clicked build")
            # if y > SCREEN_HEIGHT - 
        # clicked on world
        elif x > worldWidth or y > worldHeight:
            # print ("clicked outaaa of bounds!")
            if button == 3:
                conf.selectedThing = None
        else:
            column = x // Tile.tileSize
            row = y // Tile.tileSize
            tile = Tile.tilesXY[column, row]
            if button == 1:
                if conf.selectedThing == "buildWall":
                    placeStructureBlueprint(tile)
                else:
                    selectThing(tile)
            if button == 2:
                print ("middle column row " + str(tile.column) + " " + str(tile.row))
            if button == 3:
                if conf.selectedThing == "buildWall":
                    conf.selectedThing = None
                if not conf.selectedThing == None and type(conf.selectedThing) == Colonist:
                    print('create job')
                    conf.selectedThing.jobQueue.clear()
                    conf.selectedThing.job = Job.createJob ('move', tile, 'New', tile, conf.selectedThing, conf.selectedThing.team) 
                    conf.selectedThing.jobQueue.insert(0, conf.selectedThing.job)
                else:
                    # drawPathValues(pathTiles, screen, Tile.tileSize)
                    pass
        

def selectThing(tile):
    if tile.colonistStanding != None:
        if conf.selectedThing:
            conf.selectedThing = tile.colonistStanding
            # print(type(conf.selectedThing))
        conf.selectedThing = tile.colonistStanding
    elif not tile.item == None:
        conf.selectedThing = tile.item
    elif not tile.structure == None:
        conf.selectedThing = tile.structure
    else:
        conf.selectedThing = None
        

def getButtonText():
    button3 = Button.buttons[2]
    if conf.selectedThing == None:
        button3.text = ""
    else:
        if isinstance(conf.selectedThing, Colonist):
            if conf.selectedThing.job:
                button3.text = conf.selectedThing.firstName + " " + str(conf.selectedThing.health) + " " + conf.selectedThing.job.jobType + " " + conf.selectedThing.job.status + " rounds " + str(conf.selectedThing.weapon.roundsInMagazine) + "/" + str(conf.selectedThing.weapon.magazineSize) + " reload " + str(conf.selectedThing.weapon.reloadState)
            else:
                button3.text = conf.selectedThing.firstName + " " + str(conf.selectedThing.health)
        elif isinstance(conf.selectedThing, Structure): 
            button3.text = conf.selectedThing.typeOf  + " Finished " + str(conf.selectedThing.completed) + " Health " + str(conf.selectedThing.health)
        # elif isinstance(conf.selectedThing, Item):
        #     pass
        else:
            button3.text = ""


    