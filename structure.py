import random

class Structure:
	structures = []
	index = 0
	def __init__(self, position, typeOf, image, cost, buildCap = 10, state=0, completed = False):
		self.position = position
		self.typeOf = typeOf
		self.cost = cost
		self.state = state
		self.completed = completed
		self.image = image
		self.buildCap = buildCap
		self.structures.append(self)
		self.index += 1
		self.position.structure = self

	def structureManager():
		for structure in Structure.structures:
			if not structure.completed:
				if structure.state >= structure.buildCap:
					structure.completed = True
					print("complete")
					

class WoodenWall(Structure):
		def __init__(self, position, typeOf = "woodenWall", image = "woodenWall", cost = 100, health = 100, buildCap = 20, state=0, completed = False):
			super().__init__(position, typeOf, image, cost, buildCap, state, completed)
			self.health = health

	
