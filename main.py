import pygame
import random
import time
import math
from pygame.locals import *
from colonist import Colonist
from tile import Tile
from draw import *
from UI import *
from conf import *
from arbiter import arbiter
from shoot import moveProjectile
from build import *

def main():
    screen = pygame.display.set_mode([SCREEN_WIDTH, SCREEN_HEIGHT])
    conf.screen = screen
    images = loadImages(Tile.tileSize)
    tiles = Tile.createTiles(screen, images, tileTypes)
    Build.spawn_random_structures(Conf.number_of_structures)
    Colonist.spawnColonist(Tile.tileSize)
    makeUIElements()
    running = True
    flipMark = False
    moveScreenState = False
    while running:
        # pygame.time.delay(50)
        # for loop through the event queue
        for event in pygame.event.get():
            # Check for KEYDOWN event
            if event.type == KEYDOWN:
                # EXIT
                if event.key == K_ESCAPE:
                    running = False
                # PAUSE
                if event.key == K_SPACE:
                    if conf.paused == False:
                        conf.paused = True
                    else:
                        conf.paused = False
                if event.key == K_a:
                    moveScreenState = "right"
                if event.key == K_d:
                    moveScreenState = "left"
                if event.key == K_w:
                    moveScreenState = "down"
                if event.key == K_s:
                    moveScreenState = "up"
            elif event.type == KEYUP:
                if event.key == K_a or event.key == K_d or event.key == K_w or event.key == K_s:
                    moveScreenState = False
            # Check for QUIT event. If QUIT, then set running to false.
            elif event.type == QUIT:
                running = False
            elif event.type == MOUSEBUTTONUP:
                    mousePosition = pygame.mouse.get_pos()
                    detectClick(mousePosition, event.button)                   
            elif event.type == MOUSEWHEEL:
                screen, flipMark = zoomCamera(event.y, screen, images)

        if not moveScreenState == False: 
            flipMark = moveScreen(moveScreenState, screen, images)
        if not conf.paused:
            arbiter()
            Structure.structureManager()
            Colonist.colonistManager(images)
            if len(colonistsToMove) > 0:
                Colonist.traversePath(colonistsToMove)
            for projectile in Projectile.projectiles:
                moveProjectile(projectile, Tile.tileSize)
        flipMark = redrawRects(screen, images, flipMark, clock, pygame.mouse.get_pos())
        clock.tick(FPS)
    pygame.quit()

    
main()   
