import pygame
import random
from conf import *
import astar as Astar
import math

class Tile:
	nTiles = 0
	tiles = []
	tilesXY = {}
	tileSize = 64
	relCornerY = 0
	relCornerX = 0
	tileNumber = 0
	def __init__(self, column, row, image, tileType, content, colonistStanding = None, item = None, structure = None, vegetation = [], tileNumber = None, colonistDestination = None ):
		self.tileNumber = Tile.nTiles
		self.row = row
		self.column = column
		self.image = image
		self.tileType = tileType
		self.content = content
		self.colonistStanding = colonistStanding
		self.item = item
		self.structure = structure
		self.vegetation = vegetation
		self.colonistDestination = colonistDestination
		Tile.nTiles += 1
		self.tiles.append(self)
		self.tilesXY[(column, row)] = (self)

	def changeTile(tile, category, action, item):
		tilesChanged.append(tile)
		content = tile.content
		if category == "content" and action == "append":
			content.append(item)
		if category == "content" and action == "remove":
			# if item in content:
			content.clear()

	def createTiles(screen, images, tileTypes):
		x = 0
		surface = screen
		tileSize = Tile.tileSize
		tile = pygame.Surface((tileSize, tileSize))
		tile.set_colorkey((255, 255, 255), pygame.RLEACCEL)
		rect = tile.get_rect()
		for column in range(conf.columns):
			for row in range(conf.rows):
				tileType = random.choice(list(tileTypes))
				# tileType = "water"
				image = images[tileTypes[tileType]["image"]]
				tile.blit(image, rect)
				surface.blit(tile, (column*tileSize, row*tileSize))
				name = "tile" + str(x)
				{name: Tile(column, row, image, tileType, [])}
				x = x + 1
		pygame.display.flip()
		return Tile.tiles

	def getSurroundingTiles(currentTile):
		neighborXY = [(currentTile.column + 1, currentTile.row + 1), (currentTile.column + 1, currentTile.row), (currentTile.column + 1, currentTile.row - 1),
					(currentTile.column, currentTile.row + 1),(currentTile.column, currentTile.row - 1),
					(currentTile.column - 1, currentTile.row + 1), (currentTile.column - 1, currentTile.row), (currentTile.column - 1, currentTile.row -1)]
		surroundingTiles = []
		for i in neighborXY:
			if i in Tile.tilesXY:
				surroundingTiles.append(Tile.tilesXY[i])
		return surroundingTiles

	def getClosestWalkableTile(currentTile):
		surroundingTiles = Tile.getSurroundingTiles(currentTile)
		tileWithShortestDistance = None
		shortestDistance = 0
		while len(surroundingTiles) > 0:
			for tile in surroundingTiles:
				cost = tileTypes[tile.tileType]['cost']
				if cost < 100:
					distance = Tile.getEuclidianDistance(currentTile, tile)
					if shortestDistance == 0:
						shortestDistance = distance
						tileWithShortestDistance = tile
					if distance < shortestDistance:
						shortestDistance = distance
						tileWithShortestDistance = tile
				else:
					surroundingTiles.remove(tile)

			_, value2 = Astar.astar_search(currentTile, tileWithShortestDistance)
			if (value2):
				break
			else:
				surroundingTiles.remove(tileWithShortestDistance)

		if len(surroundingTiles) == 0:
			print("no path")
			return None
		return tileWithShortestDistance
		
	def getVector(tile1, tile2):
		return pygame.Vector2(tile2.column - tile1.column, tile2.row - tile1.row)

	def getVectorDistance(tile1, tile2):
		return pygame.Vector2(tile2.column - tile1.column * Tile.tileSize, tile2.row - tile1.row * Tile.tileSize).length()

	def getEuclidianDistance(tile1, tile2):
		return math.sqrt((tile2.column - tile1.column) ** 2 + (tile2.row - tile1.row) ** 2)

	def isReachable(startTile, endTile):
		# Check that no one is on tile
		# print("na tile " + str(endTile.tileNumber) + " stoji " + )
		if endTile.colonistStanding != None:
			return False
		_, value2 = Astar.astar_search(startTile, endTile)
		if value2:
			return True
		else:
			return False

	def getRandomReachableTile(startTile):
		randomTile = None
		while randomTile == None:
			x = random.choice(Tile.tiles)
			if Tile.isReachable(startTile, x):
				randomTile = x
		return randomTile

	def getTileByCoords(column, row):
		# print(str(conf.columns - 1) + " " + str(column))
		# print(str(conf.rows - 1) + " " + str(row))
		if column >= 0 and row >= 0:
			if column <= conf.columns - 1 and row <= conf.rows - 1:
				return Tile.tilesXY[column, row]
			else:
				print("more than max")
		else:
			print("less than 0")
		return None

	def getTileByVisualPosition(tileTuple):
		width, height = tileTuple
		column = int(width // Tile.tileSize)
		row = int(height // Tile.tileSize)
		return Tile.getTileByCoords(column, row)

