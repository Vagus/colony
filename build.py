from structure import *
import random
from tile import Tile
from conf import *
import pygame
from job import Job

class Build:
	
	def spawn_random_structures( number_of_structures):
		for i in range(number_of_structures):
			random_tile = random.choice(Tile.tiles)
			if random_tile.structure == None:
				Build.placeFinishedStructure(random_tile)

	def placeFinishedStructure(tile):
		wall = WoodenWall(tile)
		wall.completed = True
	
def build():
	conf.selectedThing = "buildWall"

def placeStructureBlueprint(tile):
	wall = WoodenWall(tile)
	job = Job("build", wall, 'new')


def advanceConstruction(structure, amount):
	if not structure.state >= structure.buildCap:
		structure.state = structure.state + amount
		return False
	else:
		return True